<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    

    <!-- Fonts -->
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ URL::to('src/css/main.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>   
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="screen" />
</head>
<body style="width: 100%; 
            height: 400%; 
            top: 0; 
            left: 0;
            background: url({{ Storage::url('public/guest_bg.jpg') }});
            background-attachment: fixed; ">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel fixed-top">
            <div class="container">



                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li>
                            <a class="navbar-brand" href="{{ url('/home') }}">
                                <i class="fa fa-home fa-2x" aria-hidden="true"></i> 
                                {{ config('app.name', 'Laravel') }}
                            </a>&nbsp;&nbsp;
                        </li>
                        @if(Auth::check())
                            <li>
                                <a class="navbar-brand" href="{{ route('profile', ['slug' => Auth::user()->slug ]) }}">
                                <i><img src="{{Storage::url(Auth::user()->avatar)}}" width="30px" height="30px" class="img-circle"/></i>
                                
                                {{ Auth::user()->name }}
                                 </a>
                            </li>   
                            
                            <li>
                               <form action="{{ route('search.user') }}" method="post"class="card card-sm">
                                <div class="card-body row no-gutters align-items-center">
                                    
                                    <!--end of col-->
                                    
                                        {{ csrf_field() }}
                                        <div class="col">
                                            <input type="text" name="search" id="search" class="form-control" placeholder="find a friend">
                                        </div>
                            
                                        <!--end of col-->
                                        <div class="col-auto">
                                            <button class="btn btn-lg btn-success" type="submit">Search</button>
                                        </div>
                                    
                                    <!--end of col-->
                                </div>
                            </form>

                           </li>
                          
                        @endif
                    </ul>
                    
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="navbar-brand" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else

                            <?php
                                   $notes_friendship = DB::table('users')
                                        ->leftJoin('friendships', 'users.id', 'friendships.user_requested')
                                    ->where('user_requested', Auth::user()->id)
                                           ->where('status',0)
                                           ->orderBy('friendships.created_at', 'desc')
                                    ->get();      
                            ?>

                            <li class="nav-item dropdown">
                                <a class="navbar-brand"  href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                                    <span class="badge" style="background: red;position: relative;top: -15px;left: -10px">      
                                      {{$notes_friendship->count()}}
                                    </span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">     
                                    <div class="box">                          
                                     @foreach($notes_friendship as $note_friendship)
                                         <li class="">                                
                                            <a class="dropdown-item " href=" {{url('/profile')}}/{{App\User::find($note_friendship->requester)->slug}}">
                                                <b style="color: green">{{App\User::find($note_friendship->requester)->name}} 
                                                </b>
                                            </a>
                                            <a  href="{{url('/')}}/accept/{{App\User::find($note_friendship->requester)->name}}/{{App\User::find($note_friendship->requester)->id}}" class="btn btn-success">Confirm
                                                </a>

                                                <a  href="{{url('/')}}/requestRemove/{{App\User::find($note_friendship->requester)->id}}" class="btn btn-danger">Remove 
                                                </a>
                                                <p>-------------------------</p>
                                         
                                        </li>

                                     @endforeach
                                 </div>
                                      
                                </ul>
                            </li>

                            <?php
                                   $notes_messages = DB::table('users')
                                        ->leftJoin('messages', 'users.id', 'messages.receiver')
                                    ->where('receiver', Auth::user()->id)
                                    ->orderBy('messages.created_at', 'desc')
                                    ->get();
                            ?>

                            <li class="nav-item dropdown">
                                <a class="navbar-brand"  href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-comments fa-2x" aria-hidden="true"></i>
                                    <span class="badge" style="background: red;position: relative;top: -15px;left: -10px">      
                                      {{ DB::table('users')
                                        ->leftJoin('messages', 'users.id', 'messages.receiver')
                                    ->where('receiver', Auth::user()->id)
                                           ->where('read_at', null) //unread noti
                                           ->orderBy('messages.created_at', 'desc')
                                    ->get()->count()}}
                                    </span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu" 
                                >         <div class="box">                     
                                     @foreach($notes_messages as $note_message)
                                                                       
                                        @if($note_message->read_at == null)
                                            <li class=" list-group-item-info">
                                        @else
                                            <li >
                                        @endif
                                        
                                            <a class="dropdown-item " href="{{url('/messages')}}/{{$note_message->user_id}}">
                                                <img src="{{Storage::url(App\User::find($note_message->user_id)->avatar)}}" width="30px" height="30px" class="img-circle"/>
                                
                                                <b style="color: green">{{App\User::find($note_message->user_id)->name}} 
                                                </b>&nbsp;&nbsp;&nbsp;on&nbsp;&nbsp;&nbsp;{{$note_message->created_at }}
                                            </a>
                                        </li>
                                     @endforeach
                                 </div>
                                </ul>
                            </li>
                            <?php
                                   $notes = DB::table('users')
                                        ->leftJoin('notifications', 'users.id', 'notifications.user_logged')
                                    ->where('user_hero', Auth::user()->id)
                                           ->where('status', 1) //unread noti
                                           ->orderBy('notifications.created_at', 'desc')
                                    ->get();
                            ?>
                            <li class="nav-item dropdown">
                                <a class="navbar-brand"  href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                   <i class="fa fa-bell fa-2x" aria-hidden="true"></i>
                                    <span class="badge" style="background: red;position: relative;top: -15px;left: -10px">      {{App\notification::where('status', 1)
                                     ->where('user_hero', Auth::user()->id)
                                      ->count()}}
                                    </span>
                                </a>                              
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">     
                                    <div class="box">                          
                                     @foreach($notes as $note)
                                        <li>
                                            <a class="dropdown-item " href="{{url('/notifications')}}/{{$note->id}}">
                                                <b style="color: green"> {{ucwords($note->name) }}
                                                </b>&nbsp;{{$note->note}} &nbsp;&nbsp;{{$note->created_at}}
                                            </a>
                                        </li>
                                     @endforeach
                                 </div>
                                </ul>
                            </li>
                            <li>
                                  <a class="btn btn-lg btn-info" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </li>

                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4"><br><br><br><br>
            @yield('content')
        </main>
    </div>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{ URL::to('src/js/app.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>
