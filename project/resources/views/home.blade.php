
@extends('layouts.app')

@section('content')
 
    <div class="container">
        <div class="col-lg-4">
            <div class="" style="position: fixed; float: left; width: 200px; height: 150px;">
                <div class="card ">
                    <div class="card-header">
                        Suggested People 
                    </div>
                </div><br>
                @foreach($allUsers as $uList)<?php
                    $check = DB::table('friendships')
                                        ->where('user_requested', '=', $uList->id)
                                        ->where('requester', '=', Auth::user()->id)
                                        ->first();
                    $check2 = DB::table('friendships')
                                        ->where('requester', '=', $uList->id)
                                        ->where('user_requested', '=', Auth::user()->id)
                                        ->first();
                        
                    if($check =='' && $check2 ==''){?>
                        <div class="card" style="border-bottom:1px solid #ccc; margin-bottom:15px">
                            <div class=" pull-left card-header">
                                <h3 style="margin:0px;">
                                    <a href="{{url('/profile')}}/{{$uList->slug}}">{{ucwords($uList->name)}}
                                    </a>
                                </h3>
                                <p><i class="fa fa-globe"></i> 
                                    {{$uList->location}} 
                                </p>
                                <p>
                                    <a href="{{url('/')}}/addFriend/{{$uList->id}}" class="btn btn-success btn-lg">Add to friend
                                    </a>
                                </p>
                            </div>
                        </div>
                    <?php }?><br>
                @endforeach
            </div>
        </div>
    </div>
    @include('profiles.slaves.add_post')
    
@endsection
