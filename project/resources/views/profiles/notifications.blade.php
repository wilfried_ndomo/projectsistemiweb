@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"><br><br>
                <div class="panel panel-default">
                <div class="panel-heading">{{Auth::user()->name}}</div>

                <div class="panel-body">
                    <div class="col-sm-12 col-md-12">
                        @if(session()->has('msg'))
                                    <p class="alert alert-success">
                                        {{session()->get('msg')}}
                                    </p>
                        @endif

                        @foreach($notes as $note)

                        <div class="row" style="border-bottom:1px solid #ccc; margin-bottom:15px">
                            <ul>
                                <li>
                                    <p><a href="" style="font-weight: bold; color: green">{{$note->name}}</a>&nbsp;{{$note->note}}</p>
                                </li>

                            </ul>                         

                        </div>
                        @endforeach
                    </div>

                </div>
            </div>

     </div>
        </div>
    </div>
</div>
@endsection
