@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit your profile.</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="" action="{{ route('profile.update') }}" method="post" enctype="multipart/form-data">
                      {{ csrf_field() }}

                      <div class="form-group">
                        <label for="avatar">Upload Profile image</label>
                        <input type="file" name="avatar" class="form-control" accept="image/*" >
                      </div>

                      <div class="form-group">
                        <label for="cover">Upload Profile cover</label>
                        <input type="file" name="cover" class="form-control" accept="image/*" >
                      </div>

                      <div class="form-group">
                        <label for="location">location</label>
                        <input type="text" name="location" value="{{ $info->location }}" class="form-control" >
                      </div>

                      <div class="form-group">
                        <label for="location">birthday</label>
                        <input type="date" name="date" value="{{ $info->birthday }}" class="form-control" >

                      </div>

                      <div class="form-group">
                        <label for="location">phone</label>
                        <input type="integer" name="phone" value="{{ $info->phone }}" class="form-control" >
                      </div>

                     

                      <div class="form-group">
                        <label for="location">About me</label>
                        <input type="text" name="about" value="{{ $info->about }}" class="form-control" >
                      </div>

                      <div class="form-group">
                        <p class="text-center" >
                          <button class="btn btn-primary btn-lg" type="submit" >
                            Save your informations
                          </button>
                        </p>
                      </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
