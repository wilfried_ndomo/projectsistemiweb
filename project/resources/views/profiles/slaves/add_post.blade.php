<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card">
                <div class="card-body">
                    
                      {{ csrf_field() }}

                      @include('includes.message-block')
                      <section class="new-post">
                        <div class="col-md-offset">
                          <form  action="{{ route('post.create') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                              <textarea class="form-control" name="body" id="new-post" rows="4" placeholder="Share what you think"></textarea>
                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-submit pull-right">  Create Post
                            </button>
                            <input type="hidden" value="{{ Session::token() }}" name="_token">
                          </form>
                        </div>
                      </section>
                </div>
              </div>
              @foreach($posts as $post)<?php
              $check = DB::table('friendships')
                                        ->where('user_requested', '=', $post->user->id)
                                        ->where('requester', '=', Auth::user()->id)
                                        ->where('status','=',1)
                                        ->first();
              $check2 = DB::table('friendships')
                                        ->where('requester', '=', $post->user->id)
                                        ->where('user_requested', '=', Auth::user()->id)
                                        ->where('status','=',1)
                                        ->first();
                      
              if(($check !='' || $check2 !='') || ($post->user->id == Auth::user()->id) ){?>
              <br>
              <section class="row posts">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <article class="post" data-postid="{{ $post->id }}" 
                                    id="panel-post-{{ $post->id }}">
                          <div class="alert alert-success" role="alert">  
                              <a class="" href="{{url('/profile')}}/{{$post->user->slug}}">
                                  <img src="{{Storage::url($post->user->avatar)}}" width="30px" height="30px" class="img-circle"/>
                                             {{ $post->user->name }}
                                  </a>
                                    <i>
                                      {{ $post->created_at->diffForHumans() }} 
                                    </i>  
                                    <p>
                                      {{ $post->body }}
                                    </p><hr>
          
                                    <i class="fa fa-heart" aria-hidden="true"></i>&nbsp;
                                    <span>
                                        {{$post->getLikeCount()}} Likes                        
                                      &nbsp;|&nbsp;
                                      {{$post->getDislikeCount()}}
                                       DisLikes
                                       </span>
                                    </div>
                                    <div class="interaction">
                                      <a onclick="myLike(event)" class="btn btn-primary" 
                                          href="javascript:;" >
                                        <i class="fa fa-thumbs-up" aria-hidden="true"></i> {{ Auth::user()->likes()->where('post_id', $post->id)->first() ? Auth::user()->likes()->where('post_id', $post->id)->first()->like == 1 ? 'You like this post' : 'Like' : 'Like'  }}
                                      </a>
                                      <a onclick="myLike(event)" class="btn btn-primary" href="javascript:;" >
                                      <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                        {{ Auth::user()->likes()->where('post_id', $post->id)->first() ? Auth::user()->likes()->where('post_id', $post->id)->first()->like == 0 ? 'You don\'t like this post' : 'Dislike' : 'Dislike'  }}
                                      </a>
                                      @if(Auth::user() == $post->user)
                                          <a class="btn btn-danger" href="{{ route('post.delete', ['post_id' => $post->id]) }}">
                                            <i class="fa fa-trash-o fa-lg"></i> Delete
                                          </a>
                                      @endif
                                    </div><br>
                                    <?php $comments = $post->comments()->get();?>
                                    <?php if($comments->count() == 0){?>
                                    <p>0 comments, <i> be the first to comment it</p>
                                    <?php }else {
                                      if($comments->count() == 1){?>
                                          <p>1 comment</p>
                                      <?php }else {?>
                                          <p>{{ $comments->count() }} comments</p>
                                      <?php }?>
                                   <?php }?>
                                    
                                  </article>
                                  <div>

<!------------------------------------Begin display comments------------------------->                                  
                                  <ul class="list-group box_comment">    
                                  @foreach($comments as $comment)
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<li class="list-group-item list-group-item-success">

                                      @if(Auth::user() == $comment->user)
                                                                   
                                     
                                      <button onclick="location.href='{{ route('comment.delete', ['comment_id' => $comment->id]) }}'" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      @endif
                                      <a class="" href="{{url('/profile')}}/{{$comment->user->slug}}">
                                                <img src="{{Storage::url($comment->user->avatar)}}" width="30px" height="30px" class="img-circle"/>
                                             {{ $comment->user->name }}
                                       </a>
                                         <i>{{ $comment->created_at->diffForHumans() }} </i> <br>


                                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$comment->comment}}</li>

                                  
                                  @endforeach
                                </ul>
                                  </div>
<!------------------------------------Begin comments------------------------->

                                  <form action="{{ route('comment.create') }}" method="POST" id="comment_form">

                                    {{ csrf_field() }}

                                    <div class="form-group">
                                    </div>

                                    <div class="form-group">
                                      <textarea name="comment_content" id="comment_content" class="form-control" placeholder="Enter Comment" rows="1"></textarea>
                                    </div>

                                    <div class="form-group">
                                      <input type="hidden" name="post_id" id="comment_id" 
                                      value="{{$post->id}}" />
                                      <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />
                                    </div>

                                  </form>

                                  <input type="hidden" value="{{ Session::token() }}" name="_token">
                                  <span id="comment_message"></span>
                                  <br />
                                  <div id="display_comment"></div>
<!------------------------------------End comments-------------------------------------->
                                </div>
                              </div>
                            </div>
                          </section>
                          <?php }?>
                        @endforeach 

                        <script>
                          var token = '{{ Session::token() }}';
                          var urlLike = '{{ route('like') }}';
                        </script>
                        <script src="{{ URL::to('src/js/app.js') }}"></script>
        </div>
    </div>
</div>
    