<br><br><br><br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
<div class="">
                                       
                    <div class="panel-body">

                        <div class="col-sm-12 col-md-12"> 
                            <?php $counter = 0 ?>
                            @foreach($allUsers as $uList)<?php
                                $check = DB::table('friendships')
                                        ->where('user_requested', '=', $uList->id)
                                        ->where('requester', '=', Auth::user()->id)
                                        ->first();
                                $check2 = DB::table('friendships')
                                        ->where('requester', '=', $uList->id)
                                        ->where('user_requested', '=', Auth::user()->id)
                                        ->first();
                        
                                if($check =='' && $check2 ==''){
                                    $counter = 1;?>
                                    <div class="row" style="border-bottom:1px solid #ccc; margin-bottom:15px">
                                        <div class="col-md-2 pull-left">
                                            <img src="{{Storage::url($uList->avatar) }}" width="80px" height="80px" class="img-rounded"/>
                                        </div>
                                        <div class="col-md-7 pull-left">
                                            <h3 style="margin:0px;"><a href="{{url('/profile')}}/{{$uList->slug}}">{{ucwords($uList->name)}}</a></h3>
                                            <p><i class="fa fa-globe"></i> {{$uList->location}} </p>
                                            <p>{{$uList->about}}</p>
                                            <p><a href="{{url('/')}}/addFriend/{{$uList->id}}" class="btn btn-success">Add to friend</a></p>
                                        </div>
                                    </div>
                                <?php }
                                ?>
                                   
                            @endforeach
                            <?php if($counter == 0){ ?>
                                <p>Nessuna suggessione</p>
                            <?php }?>
                        </div>
                    </div>
                    
                </div>
                </div>
        </div>
    </div>
</div>