<div class="card panel panel-default" style="position: fixed; float: left;
width: 200px;
height: 150px;">
  <div class="card-header">
  {{ $user->name }} - {{ $user->profile->location }}<br>
  birthday - {{ $user->profile->birthday }}<br>
  phone - {{ $user->profile->phone }}<br>
  </div>
  <div class="card-header">
    About me.
  </div>
  <div class="card-body">
    <div class="form-group">
      <p class="text-center" >
        {{ $user->profile->about }}
      </p>
    </div>
  </div>
</div>
