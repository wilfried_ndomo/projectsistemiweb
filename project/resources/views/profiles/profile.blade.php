@extends('layouts.app')

@section('content')

<div class="container">
  <div class="raw">
    <div class="col-lg-12 text-white " style="width: 100%; height: 40%; top: 0; left: 0;
      background: url({{ Storage::url($user->cover) }}) no-repeat center top; position: fixed; z-index: -1;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;">
      </div>
  </div>
  <br><br><br>
  <div class="col-lg-4">
    <div class="" style="position: fixed; float: left;
    width: 200px;
    height: 150px;">
      <div class="panel-body">
          <img src="{{ Storage::url($user->avatar) }}" width="150px" height="150px" style="border-radius: 10%;" alt="">
      </div><br>
      
      <div>
        <h2 class="list-group-item d-flex justify-content-between align-center">
                        
                      {{$user->name}} {{$user->username}}'s profile
                      
        </h2>
      </div>
      @if(Auth::id() == $user->id)
      
      
      <div class="list-group">
        <a class=" btn btn-info " href="{{ route('profile.edit') }}"><i class="fa fa-cog fa-fw" aria-hidden="true"></i>&nbsp; Edit your profile</a>
      </div><br>
      <div class="list-group">
            <a class="btn btn-lg btn-info" href="{{url('/messages')}}">
              <i class="fa fa-comment fa-2x" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
              Chat with frinds
            </a>
      </div><br>
      <div class="list-group">
        <a class=" btn btn-lg btn-info" href="{{url('/friends')}}"><i class="fa fa-users fa-2x" aria-hidden="true"></i>&nbsp;All Your friends</a>
      </div>
      @else 
        <?php
        $check = DB::table('friendships')
                                        ->where('user_requested', '=', $user->id)
                                        ->where('requester', '=', Auth::user()->id)
                                        ->where('status','=',1)
                                        ->first();
        $check2 = DB::table('friendships')
                                        ->where('requester', '=', $user->id)
                                        ->where('user_requested', '=', Auth::user()->id)
                                        ->where('status','=',1)
                                        ->first();
        $check3 = DB::table('friendships')
                                        ->where('user_requested', '=', $user->id)
                                        ->where('requester', '=', Auth::user()->id)
                                        ->first();
        $check4 = DB::table('friendships')
                                        ->where('requester', '=', $user->id)
                                        ->where('user_requested', '=', Auth::user()->id)
                                        ->first();
                      
        if($check !='' || $check2 !='' ){?>
          <div class="list-group">
            <a href="{{url('/requestRemove')}}/{{$user->id}}" class="btn btn-danger">
              Remove from  friends</a>
            </div><br>

          <div class="list-group">
            <a class="btn btn-lg btn-info" href="{{url('/messages')}}/{{$user->id}}">
              <i class="fa fa-comment fa-2x" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
              Send a message
            </a>
          </div>
        <?php }
        else{
          if($check3 =='' && $check4 ==''){?>
            <div class="list-group">
              <a href="{{url('/')}}/addFriend/{{$user->id}}" class="btn btn-success">
                Add  to  friends
              </a>
            </div>
                     
          <?php }?>
        <?php }?>
      @endif

      <br><br>

<div class="list-group">
  <p class=" list-group-item">
    <i class="fa fa-globe"></i>&nbsp; &nbsp;
     {{ $user->profile->location }}
  </p>

  <p class=" list-group-item">
    <i class="fa fa-birthday-cake" aria-hidden="true"></i>&nbsp;&nbsp; 
      {{ $user->profile->birthday }}
  </p>

  <p class=" list-group-item">
    <i class="fa fa-phone" aria-hidden="true"></i>&nbsp; &nbsp;
     {{ $user->profile->phone }}
  </p>

  <p class=" list-group-item">
    <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;&nbsp; 
      {{ $user->profile->about }}
  </p>
</div>
    </div>
  </div>
</div>
<br><br><br><br><br><br><br><br><br><br><br>
@include('profiles.slaves.add_post')

@stop
