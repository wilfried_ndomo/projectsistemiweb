@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"><br><br>
                


                  <div class="panel panel-default">
                <div class="panel-heading">{{Auth::user()->name}} Your Friends</div>

                <div class="panel-body">
                    <div class="col-sm-12 col-md-12">
                        @if(session()->has('msg'))
                                    <p class="alert alert-success">
                                        {{session()->get('msg')}}
                                    </p>
                        @endif

                        @foreach($friends as $uList)

                        <div class="row" style="border-bottom:1px solid #ccc; margin-bottom:15px">
                            <div class="col-md-2 pull-left">
                                <img src="{{Storage::url($uList->avatar) }}" width="80px" height="80px" class="img-rounded"/>
                            </div>

                            <div class="col-md-7 pull-left">
                              
                                <h3 style="margin:0px;"><a href="{{url('/profile')}}/{{$uList->slug}}">{{ucwords($uList->name)}}</a></h3>
                                <p>{{$uList->email}} </p>

                               <p>

                                        <a  href="{{url('/requestRemove')}}/{{$uList->id}}" class="btn btn-danger">Unfriend </a></p>
                            </div>                           

                        </div>
                        @endforeach
                    </div>

                </div>
            </div>

            </div>
        </div>
    </div>
</div>
@endsection
