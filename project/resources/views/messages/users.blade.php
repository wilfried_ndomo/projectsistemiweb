
<div class="col-md-3" >
    
        <div class="card">
             <div class="card-header" >Talk with your friends</div>

            <div class="card-body">
                @foreach ($users as $user)
                    <a class="list-group-item d-flex justify-content-between align-items-center" href="{{url('/messages')}}/{{$user->id}}">
                        <img src="{{Storage::url(App\User::find($user->id)->avatar)}}" width="30px" height="30px" class="img-circle"/>
                    	{{$user->name}} {{$user->username}}
                    	@if(isset($unread[$user->id]))
                    		<span class="badge badge-pill badge-primary">{{$unread[$user->id]}}</span>
                    	@endif
                    </a>
                @endforeach                    
            </div>
        </div>
</div>