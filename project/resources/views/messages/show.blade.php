@extends('layouts.app')

@section('content')
<br><br><br><br><br>
<div class="container" style=" height: 50%; position: fixed; ">
    <div class="row justify-content-center">

        @include('messages.users',['users'=>$users,'unread'=>$unread])

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                     <a class="list-group-item align-items-center" href="{{url('/profile')}}/{{$user->slug}}">
                    <img src="{{Storage::url(App\User::find($user->id)->avatar)}}" width="30px" height="30px" class="img-circle"/>
                    {{ $user->name }} {{ $user->username }}
                    </a>
                </div>

                <div class="card-body conversations box_message" id="my_div">
                	@if($messages->hasMorePages())
                		<div class="text-center">
                			<a href="{{ $messages->nextPageUrl()}}">
                				See more ...
                			</a>
                		</div>

                	@endif
                	@foreach($messages as $msg)
                		<div class="row">
                			<div class="col-md-10 {{$msg->from->id !== $user->id ? 'offset-md-2 text-right': ''}}">
                                @if($msg->from->id !== $user->id)
                                <div class="alert alert-success" role="alert">
                                <p>
                                    {{ $msg->msg }} | 
                                    <img src="{{Storage::url(App\User::find($msg->from->id)->avatar)}}" width="30px" height="30px" class="img-circle"/>
                                    
                                </p>
                                </div>
                                
                                @else
                                <div class="alert alert-primary" role="alert">
                                    <img src="{{Storage::url(App\User::find($user->id)->avatar)}}" width="30px" height="30px" class="img-circle"/>
                                    | {{ $msg->msg }}</p>
                                </div>
                                @endif                              
                			</div><hr>               			
                		</div>
                	@endforeach               	
                	@if($messages->hasMorePages())
                		<div class="text-center">
                			<a href="{{ $messages->previousPageUrl()}}">
                				See previous messages ...
                			</a>
                		</div>

                	@endif             

                	<form action="" method="post">

                		  {{ csrf_field() }}
                		<div class="form-group ">

                			<input name="content" placeholder="send a message" class="form-control {{$errors->has('content') ? 'is-invalid' : ''}}">
                			@if($errors->has('content'))
                				<div class="invalid-feedback">{{implode (',',$errors->get('content'))}}		
                				</div>
                			@endif
                		</div>
                		<button class="btn btn-primary" type="submit">Send</button>	
                	</form>	
                </div>
            </div>
        </div>
    </div>
</div>
