<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();
route::group(['middleware' => 'auth'], function(){
Route::get('/home',[
    'uses' => 'PostController@getDashboard',
    'as' => 'home',
    'middleware' => 'auth'
  ]);

route::get('/profile/{slug}', [
    'uses' => 'ProfilesController@index',
    'as' => 'profile'
  ]);

route::get('/profile/edit/profile', [
    'uses' => 'ProfilesController@edit',
    'as' => 'profile.edit'
  ]);

route::post('/profile/update/profile', [
    'uses' => 'ProfilesController@update',
    'as' => 'profile.update'
  ]);

Route::get('/findFriends', 'ProfilesController@findFriends');
Route::get('/addFriend/{id}', 'ProfilesController@sendRequest');
Route::get('/requests', 'ProfilesController@requests');
Route::get('/accept/{name}/{id}', 'ProfilesController@accept');
Route::get('/friends', 'ProfilesController@friends');
Route::get('/requestRemove/{id}', 'ProfilesController@requestRemove');
Route::get('/notifications/{id}','ProfilesController@notifications');
Route::get('/messages','MessageController@index');
Route::get('/messages/{user}','MessageController@show');
Route::post('/messages/{user}','MessageController@store');
Route::post('/search',[ 
    'uses' => 'MessageController@search',
    'as' => 'search.user']);

  
Route::post('/createpost', [
    'uses' => 'PostController@postCreatePost',
    'as' => 'post.create'
    
]);

Route::get('/post-delete/{post_id}', [
    'uses' => 'PostController@getDeletePost',
    'as' => 'post.delete'
  ]);

Route::post('/like', [
    'uses' => 'PostController@postLikePost',
    'as' => 'like'
  ]);

Route::post('/comment', [
    'uses' => 'CommentController@postCommentPost',
    'as' => 'comment.create'
  ]);

Route::get('/comment-delete/{comment_id}', [
    'uses' => 'CommentController@getDeleteComment',
    'as' => 'comment.delete'
  ]);

});
