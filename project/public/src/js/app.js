var postId = 0;
//var postBodyElement = null;

$("#my_div").scrollTop($("#my_div")[0].scrollHeight);

function myLike(event) {
	event.preventDefault();
	postId = event.target.parentNode.parentNode.dataset['postid'];
	
	var isLike = event.target.previousElementSibling == null;
	$.ajax({
		method: 'POST',
		url: urlLike,
		data: {isLike: isLike, postId: postId, _token: token}
	})
		.done(function(response) {
		
		    event.target.innerText = isLike ? event.target.innerText == 'Like' ? 'You like this post' : 'Like' : 
		    event.target.innerText == 'Dislike' ? 'You don\'t like this post' : 'Dislike';
		    if (isLike) {
		        event.target.nextElementSibling.innerText = 'Dislike';
		    } else {
		        event.target.previousElementSibling.innerText = 'Like';
		    }
		  
		  if((typeof(response.l) != "undefined") && (typeof(response.d) != "undefined") ){
		    $('#panel-post-'+postId+' span').html(response.l +' Likes | ' + response.d + ' Dislikes');
		  }
        	});
};
