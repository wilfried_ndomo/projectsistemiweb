<?php

namespace App\Repository;
use App\friendship;
use App\User;
use Auth;
use Session;
use App\notification;
use App\message;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class Conversation{

	private $user,$message;

	public function __construct(User $user,Message $message){

		$this->user = $user;
		$this->message = $message;
	}

	public function getConversations($userId ){

		$friends1 = DB::table('friendships')
                  ->leftJoin('users','users.id','friendships.user_requested')
                  ->where('status',1)
                  ->where('requester',$userId)
                  ->get();
                  
        $friends2 = DB::table('friendships')
                  ->leftJoin('users','users.id','friendships.requester')
                  ->where('status',1)
                  ->where('user_requested',$userId)
                  ->get();
        return $conversations = array_merge($friends1->toArray(),$friends2->toArray()); 
	}

	
	public function createMesssage($from, $to,$content)
	{
		return $this->message->newQuery()->create([
			'user_id' => $from,
			'receiver' => $to,
			'msg' => $content,
			'read' => 0
		]);
	}


	public function getMessageFor($from,$to)
	{
		return $this->message->newQuery()
		->whereRaw("((user_id = $from AND  receiver = $to) OR (user_id = $to AND receiver = $from))")
		->orderBy('created_at','ASC')
		->with([
			'from' => function($query){ return $query->select('name','id');}
		]);

	}

	//count message unread for single user
	public function unReadCount($userId)
	{
		$r=0;
		return $this->message->newQuery()
				->where('receiver',$userId)
				->groupBy('user_id')
				->selectRaw('user_id ,COUNT(id) AS count')
				->whereRaw('read_at IS NULL')
				->get()
				->pluck('count','user_id');
				
	}

	public  function readAllFrom($from,$to){

		$this->message->where('user_id',$from)
					  ->where('receiver',$to)
					  ->update(['read_at' => Carbon::now()]);

	}


}