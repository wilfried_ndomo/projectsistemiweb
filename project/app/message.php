<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class message extends Model
{
   protected $fillable = [
		'user_id','receiver','msg','read_at'
	];
	public function from(){
		return $this->belongsTo(User::class,'user_id');
	} 
}
