<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	private $like_count = null;
	private $dislike_count = null;



    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function likes()
    {
        return $this->hasMany('App\Like');
    }
    public function Comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function getLikeCount(){
        if ($this->like_count == null){
            $this->like_count = $this->likes()->where('like', 1)->count();
        }
        return $this->like_count;
    }
    public function getDislikeCount(){
        if ($this->dislike_count == null){
            $this->dislike_count = $this->likes()->where('like', 0)->count();
        }
        return $this->dislike_count;
    }
}
