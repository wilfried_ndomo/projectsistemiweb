<?php
namespace App\Http\Controllers;
use App\Like;
use App\Post;
use App\notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
class PostController extends Controller
{
    public function getDashboard()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();

        $uid = Auth::user()->id;
        $allUsers = DB::table('profiles')
                        ->leftJoin('users', 'users.id', '=', 'profiles.user_id')
                        ->where('users.id', '!=', $uid)->get();



        return view('home', ['posts' => $posts,'allUsers' => $allUsers]);
    }

    public function postCreatePost(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:1000'
        ]);

        $post = new Post();
        $post->body = $request['body'];

        $message = 'There was an error';

        if ($request->user()->posts()->save($post)) {
            $message = 'Post successfully created!';
            $uid = Auth::user()->id; 
            $friends1 = DB::table('friendships')
                          ->leftJoin('users','users.id','friendships.user_requested')
                          ->where('status',1)
                          ->where('requester',$uid)
                          ->get();
                          
            $friends2 = DB::table('friendships')
                          ->leftJoin('users','users.id','friendships.requester')
                          ->where('status',1)
                          ->where('user_requested',$uid)
                          ->get();
            $friends = array_merge($friends1->toArray(),$friends2->toArray());
            if(count($friends)>0)
            {
                foreach ($friends as  $value) 
                {
                  $notifcations = new notification;
                  $notifcations->note = 'did a new post';
                  $notifcations->user_hero = $value->id; // who is accepting my request
                  $notifcations->user_logged = $uid; // me
                  $notifcations->status = '1'; // unread notifications
                  $notifcations->save();
                # code...
                }
            }    
        }
        return redirect()->back()->with(['message' => $message]);
      
    }
    public function getDeletePost($post_id)
    {
        $post = Post::where('id', $post_id)->first();
        if (Auth::user() != $post->user) {
            return redirect()->back();
        }
        $post->delete();
        return redirect()->back()->with(['message' => ' Post Successfully deleted!']);
       
    }
    
    public function postLikePost(Request $request)
    {
        $response = array();
        $response['check'] = 0;

        $post_id = $request['postId'];
        $is_like = $request['isLike'] === 'true';
        $update = false;
        $post = Post::find($post_id);
        if (!$post) {
            return null;
        }
        $user = Auth::user();
        $like = $user->likes()->where('post_id', $post_id)->first();
        if ($like) {
            $already_like = $like->like;
            $update = true;
            if ($already_like == $is_like) {
                $like->delete();
                return null;
            }



        } else {
            $like = new Like();

        }

        if($is_like ==1){

            $msg_like="liked your post";
        }
        else
        {
            $msg_like="disliked your post";
        }

        $like->like = $is_like;
        $like->user_id = $user->id;
        $like->post_id = $post->id;
    
        if ($update) {
            $like->update();
        } else {
           $like->save();
        }

         $user_post_id=$post->user->id;

           if($user->id != $user_post_id){
                  $notifcations = new notification();
                  $notifcations->note = $msg_like;
                  $notifcations->user_hero = $user_post_id; 
                  $notifcations->user_logged = $user->id; 
                  $notifcations->status = '1'; // unread notifications
                  $notifcations->save();
            }

        $response['check'] = 1;
        $response['like_count'] = $post->getLikeCount();
        $response['dislike_count'] = $post->getDislikeCount();
       
        return response()->json(['l' => $response['like_count'], 
                                 'd' => $response['dislike_count'], 
                                 'check' => $response['check'] ]);
    }


}