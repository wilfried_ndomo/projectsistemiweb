<?php
namespace App\Http\Controllers;
use Auth;
use Session;
use App\User;
use App\Post;
use App\friendship;
use App\notification;
use Illuminate\Http\Request;
use DB;


class ProfilesController extends Controller
{
    public function index($slug){
      $user = User::where('slug', $slug)->first();
      $posts = Post::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();

      return view('profiles.profile', ['posts' => $posts,'user' => $user]);
    }

    public function edit(){
      return view('profiles.edit')->with('info', Auth::user()->profile);
    }

    public function update(Request $r){

      $this->validate($r, [
        'location' => 'required',
        'bout' => 'required/max:255',
        'phone' => 'required|digits:10',
        'date' => 'required'
      ]);

      Auth::user()->profile()->update([
        'location' => $r->location,
        'about' => $r->about,
        'birthday' => $r->date,
        'phone' => $r->phone
      ]);

      if ($r->hasFile('avatar')) {
        Auth::user()->update([
          'avatar' => $r->avatar->store('public/avatars')
        ]);
      }

      if ($r->hasFile('cover')) {
        Auth::user()->update([
          'cover' => $r->cover->store('public/covers')
        ]);
      }



      session::flash('success', 'Profile updated.');
      return redirect()->back();

    }
    ///=============================================================================
    public function findFriends()
    {
        $uid = Auth::user()->id;


        $allUsers = DB::table('profiles')
                        ->leftJoin('users', 'users.id', '=', 'profiles.user_id')
                        ->where('users.id', '!=', $uid)->get();
         return view('profiles.findFriends',compact('allUsers'));
    }

    public function sendRequest($id)
    {
        return Auth::user()->addFriend($id);
        

    } 
    public function requests()
    {

      $uid = Auth::user()->id;


       $friendRequest = DB::table('friendships')
                        ->rightJoin('users', 'users.id', '=', 'friendships.requester')
                        ->Where('status', '=', 0)
                        ->where('friendships.user_requested', '=', $uid)->get();
      return view('profiles.requests',compact('friendRequest'));
    
    }
    public function accept($name,$id)
    {

      $uid = Auth::user()->id;
      $checkRequest = DB::table('friendships')
                ->where('requester', '=', $id)
                ->Where('user_requested', '=', $uid)
                ->first();
      

      if($checkRequest)
      {
           $updateFriendship = DB::table('friendships')
                    ->where('user_requested', $uid)
                    ->where('requester', $id)
                    ->update(['status' => 1]);

          $notifcations = new notification;
          $notifcations->note = 'accepted your friend request';
          $notifcations->user_hero = $id; // who is accepting my request
          $notifcations->user_logged = $uid; // me
          $notifcations->status = '1'; // unread notifications
          $notifcations->save();

          if($notifcations)
          {
            return redirect()->back()->with('msg','Your are now Friend with'.$name);
          }
      }

    }

    public function friends()
    {

      $uid = Auth::user()->id; 
      $friends1 = DB::table('friendships')
                  ->leftJoin('users','users.id','friendships.user_requested')
                  ->where('status',1)
                  ->where('requester',$uid)
                  ->get();
                  
      $friends2 = DB::table('friendships')
                  ->leftJoin('users','users.id','friendships.requester')
                  ->where('status',1)
                  ->where('user_requested',$uid)
                  ->get();
      $friends = array_merge($friends1->toArray(),$friends2->toArray());      
      return view('profiles.friends',compact('friends'));

    }
    public function requestRemove($id)
    {

       $uid = Auth::user()->id;
       $req= DB::table('friendships')
                ->where('requester', '=', $id)
                ->Where('user_requested', '=', $uid)
                ->count();
              
       $req1= DB::table('friendships')
                ->where('requester', '=', $uid)
                ->Where('user_requested', '=', $id)
                ->count();
                
      


      if($req == 0)
      {
          
          DB::table('friendships')
                ->where('requester', '=', $uid)
                ->Where('user_requested', '=', $id)
                ->delete();
          return redirect()->back()->with('msg','Request has been delete');
      }
      else
      {
         
          DB::table('friendships')
                ->where('requester', '=', $id)
                ->Where('user_requested', '=', $uid)
                ->delete();
          return redirect()->back()->with('msg','Request has been delete');
      }
      
    }


  public function notifications($id)
  {
    $uid = Auth::user()->id;
    $notes = DB::table('notifications')
                ->leftJoin('users', 'users.id', 'notifications.user_logged')
                ->where('notifications.id', $id)
                ->where('user_hero', $uid)
                ->orderBy('notifications.created_at', 'desc')
                ->get();
    
    $updateNoti = DB::table('notifications')
                     ->where('notifications.id', $id)
                    ->update(['status' => 0]);
    return redirect()->back();
  }

}
