<?php

namespace App\Http\Controllers;
use Illuminate\Auth\AuthManager;
use App\Repository\Conversation;
use App\Http\Requests\StoreMessage;
use Auth;
use Session;
use App\User;
use App\message;
use App\friendship;
use App\notification;
use Illuminate\Http\Request;
use DB;



class MessageController extends Controller
{
  
    private $rep;
    private $auth;
    /**
    * @var AuthManager
    */

    public function __construct(Conversation $conversation, AuthManager $auth)
    {
        $this->rep = $conversation;
        $this->auth = $auth;
    } 
   
    public function index()
    {
    	return view('messages.message',[
            'users' => $this->rep->getConversations($this->auth->user()->id),
            'unread' => $this->rep->unreadCount($this->auth->user()->id)
             ]);
     
    }
    public function show(User $user)
    {
        $me = $this->auth->user();
        $messages = $this->rep->getMessageFor($this->auth->user()->id,$user->id)->paginate(50);
        $unread = $this->rep->unreadCount($this->auth->user()->id);
        if(isset($unread[$user->id]))
        {
            $this->rep->readAllFrom($user->id,$me->id);
            unset($unread[$user->id]);
        }
        return view('messages.show',[
            'users' => $this->rep->getConversations($this->auth->user()->id),
            'user' => $user,
            'messages' => $messages,
            'unread' => $unread 
        ]);     
     
    }

    public function store(User $user,StoreMessage $request)
    {
            $this->rep->createMesssage(
            $this->auth->user()->id,
            $user->id,
            $request->get('content')
       );
        return redirect()->back()->with('id',$user->id);

    }

    public function search (Request $request){
        $req=$request->get('search');
        if($req != ""){
            $users = DB::table('users')
                        ->where('name', 'like', '%'.$req.'%')
                        ->orWhere('username', 'like', '%'.$req.'%')
                        ->get();
            if(count($users)>0){
                return view('messages.search')->withDetails($users)->withQuery($req);
            }
            return view('messages.search')->withMessage("No user found");

        }
    }   
}
