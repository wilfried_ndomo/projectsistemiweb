<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Post;
use App\User;
use App\notification;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function postCommentPost(Request $request)
    {
       
        $this->validate($request, [
            'comment_content' => 'required|min:2|max:5000',
            'post_id' => 'required|max:225',
        ]);
        $user = Auth::user();
        $comment = new Comment();
        $comment->comment = $request['comment_content'];
        $comment->post_id = $request['post_id'];
        $comment->user_id = Auth::user()->id;
        $comment->save();

        $post = Post::find($comment->post_id);
        $user_post_id=$post->user->id;

        if($user->id != $user_post_id){
                  $notifcations = new notification();
                  $notifcations->note = 'commented your post';
                  $notifcations->user_hero = $user_post_id; 
                  $notifcations->user_logged = $user->id; 
                  $notifcations->status = '1'; // unread notifications
                  $notifcations->save();
            }

        return redirect()->back();
    }
    public function create()
    {
        $this->validate($request, array(
            'comment' => 'requered|min:5|max:5000',
            'post_id' => 'required|max:225',
            'user_id' => 'required|max:225',
        ));

        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->post_id = $request->post_id;
        $comment->user_id = $request->user_id;
        $comment->save();

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'comment' => 'requered|min:5|max:5000',
            'post_id' => 'required|max:225',
            'user_id' => 'required|max:225',
        ));

        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->post_id = $request->post_id;
        $comment->user_id = $request->user_id;
        $comment->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDeleteComment($comment_id)
    {
        $comment = Comment::where('id', $comment_id)->first();
        if (Auth::user() != $comment->user) {
            return redirect()->back();
        }
        $comment->delete();
        return redirect()->back()->with(['message' => 'Comment Successfully deleted!']);
    }
}
