<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\friendship;
use App\notification;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uid = Auth::user()->id;
        $allUsers = DB::table('profiles')
                        ->leftJoin('users', 'users.id', '=', 'profiles.user_id')
                        ->where('users.id', '!=', $uid)->get();
        return view('home',compact('allUsers'));               
      
    }
}
